---
layout: no-sidebar
title: Oval rings with lock by Aranea
---

# Oval rings with lock

<span class="image right"><img src="images/oval-rings.png" alt="" /></span>

Oval rings are open and ready to put on bird's leg. The edges of the rings are blunt. According to your requirements rings of other sizes can be produced. The rings are packed in plastic bags (10 pcs).

<h4>Table of sizes</h4>
<table>
<tr>
<th>Inner diameter (mm)</th>
<th>Height (mm)</th>
<th>Material</th>
<th>Features</th>
</tr>
<tr>
<td>9.0 &#215; 6.0</td>
<td>6</td>
<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ST</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>13.5 &#215; 8.0</td>
<td>6</td>
<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ST</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>18.0 &#215; 13.5</td>
<td>10</td>
<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ST</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>22.0 &#215; 12.0</td>
<td>6</td>
<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ST</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>22.0 &#215; 17.0</td>
<td>12</td>
<td>AL</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>30.0 &#215; 17.5</td>
<td>12</td>
<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ST</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>30.0 &#215; 20.0</td>
<td>20</td>
<td>AL</td>
<td>DB</td>
</tr>
<tr>
<td>30.0 &#215; 20.6</td>
<td>12</td>
<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ST</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>35.0 &#215; 17.5</td>
<td>12</td>
<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ST</td>
<td>&nbsp;</td>
</tr>
</table>

The letters code:
- **AL** - pure aluminium or aluminium-magnesium-manganese alloy
- **ST** - stainless steel
- **DB** - double number
- **TX** - with the texture on the inside surface for plasticine filling (duckling rings)  
