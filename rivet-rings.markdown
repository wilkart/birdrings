---
layout: no-sidebar
title: Rivet rings produced by Aranea
---

# Rivet rings

<span class="image right"><img src="images/rivet-rings-all.png" alt="" /></span>

The edges of the rings are blunt. The rings are packed in plastic bags (10 pcs). According to your requirements rings of other sizes can be produced.

<h4>Table of sizes</h4>
<table>
<tr>
<th>Inner diameter (mm)</th>
<th>Height (mm)</th>
<th>Material</th>
<th>Features</th>
</tr>
<tr>
<td>16.0</td>
<td>12</td>
<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ST</td>
<td></td>
</tr>
<tr>
<td>&nbsp;&nbsp;&nbsp;16.0</td>
<td>20</td>
<td>AL</td>
<td>DB</td>
</tr>
<tr>
<td>18.0</td>
<td>20</td>
<td>AL</td>
<td>DB</td>
</tr>
<tr>
<td>19.0</td>
<td>12</td>
<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ST</td>
<td></td>
</tr>
<tr>
<td>22.0</td>
<td>28</td>
<td>AL</td>
<td>DB</td>
</tr>
<tr>
<td>23.0</td>
<td>15</td>
<td>AL</td>
<td>DB</td>
</tr>
<tr>
<td>&nbsp;&nbsp;&nbsp;23.0</td>
<td>35</td>
<td>AL</td>
<td>DB</td>
</tr>
<tr>
<td>26.0</td>
<td>18</td>
<td>AL</td>
<td>DB</td>
</tr>
<tr>
<td>&nbsp;&nbsp;&nbsp;26.0</td>
<td>20</td>
<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ST</td>
<td></td>
</tr>
<tr>
<td>&nbsp;&nbsp;&nbsp;26.0</td>
<td>28</td>
<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ST</td>
<td>DB</td>
</tr>
<tr>
<td>&nbsp;&nbsp;&nbsp;26.0</td>
<td>35</td>
<td>AL</td>
<td>DB</td>
</tr>
</table>

The letters code:
- **AL** - pure aluminium or aluminium-magnesium-manganese alloy
- **ST** - stainless steel
- **DB** - double number
