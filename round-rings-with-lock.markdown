---
layout: no-sidebar
title: Round rings with lock produced by Aranea
---

# Round rings with lock

<span class="image right"><img src="images/round-ring-with-lock.png" alt="" /></span>

Round rings with lock are open and ready to put on bird's leg. The edges of the rings are blunt. According to your requirements rings of other sizes can be produced. Small rings are packed on plastic tubes (100 pcs), large rings are packed in plastic bags (10 pcs).

<h4>Table of sizes</h4>
<table>
<tr>
<th>Inner diameter (mm)</th>
<th>Height (mm)</th>
<th>Material</th>
<th>Features</th>
</tr>
<tr>
<td>7.0</td>
<td>&nbsp;8.5</td>
<td>AL</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>9.0</td>
<td>10</td>
<td>AL, ST</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>10.0</td>
<td>10</td>
<td>AL, ST</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>11.0</td>
<td>10</td>
<td>AL, ST</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>12.0</td>
<td>10</td>
<td>AL</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;&nbsp;&nbsp;12.0</td>
<td>12</td>
<td>AL</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>13.0</td>
<td>10</td>
<td>AL, ST</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>14.0</td>
<td>10</td>
<td>AL, ST</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>16.0</td>
<td>12</td>
<td>AL, ST</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;&nbsp;&nbsp;16.0</td>
<td>15</td>
<td>AL</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>18.0</td>
<td>10</td>
<td>AL</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;&nbsp;&nbsp;18.0</td>
<td>12</td>
<td>AL, ST</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>19.0</td>
<td>10</td>
<td>AL</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;&nbsp;&nbsp;19.0</td>
<td>15</td>
<td>AL</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>20.0</td>
<td>18</td>
<td>AL</td>
<td>DB</td>
</tr>
<tr>
<td>22.0</td>
<td>18</td>
<td>AL</td>
<td>DB</td>
</tr>
<tr>
<td>23.0</td>
<td>28</td>
<td>AL</td>
<td>DB</td>
</tr>
<tr>
<td>26.0</td>
<td>&nbsp;9</td>
<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ST</td>
<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LK</td>
</tr>
<tr>
<td>&nbsp;&nbsp;&nbsp;26.0</td>
<td>12</td>
<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ST</td>
</tr>
<tr>
<td>&nbsp;&nbsp;&nbsp;26.0</td>
<td>20</td>
<td>AL, ST</td>
<td>DB</td>
</tr>
<tr>
<td>30.0</td>
<td>10</td>
<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ST</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;&nbsp;&nbsp;30.0</td>
<td>12</td>
<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ST</td>
<td>&nbsp;</td>
</tr>
</table>

The letters code:
- **AL** – pure aluminium or aluminium-magnesium-manganese alloy
- **ST** – stainless steel
- **DB** – double number
- **LK** – special lock
