---
layout: post
title:  "Bird species in Poland"
date:   2022-09-02 00:00:00
categories: birds
image: /images/pic09.jpg
---

As a Polish company and the producer of bird rings for [The Ornithological Station, Museum and Institute of Zoology Polish Academy of Sciences](http://www.stornit.gda.pl/index_en.php) we present a list of bird species ringed in Poland. 
The list is helpful to choose type and size of ring to fit on similar bird species in other regions.

| La (2.0; 5.0) |
|---|
| Aegithalos caudatus |
| Certhia brachydactyla, Certhia familiaris |
| Ficedula albicollis, Ficedula hypoleuca, Ficedula parva |
| Phylloscopus bonelli, Phylloscopus borealis, Phylloscopus collybita, Phylloscopus fuscatus, Phylloscopus humei, Phylloscopus ibericus, Phylloscopus inornatus, Phylloscopus proregulus, Phylloscopus schwarzi, Phylloscopus sibilatrix, Phylloscopus trochiloides, Phylloscopus trochilus |
| Regulus ignicapilla, Regulus regulus |
| Troglodytes troglodytes L |


| Ka (2.5; 5.0) |
|---|
| Acanthis flammea, Acanthis hornemanni |
| Acrocephalus agricola, Acrocephalus dumetorum, Acrocephalus melanopogon, Acrocephalus paludicola, Acrocephalus palustris, Acrocephalus schoenobaenus, Acrocephalus scirpaceus |
| Anthus campestris, Anthus cervinus, Anthus hodgsoni, Anthus petrosus, Anthus pratensis, Anthus richardi, Anthus spinoletta, Anthus trivialis |
| Calandrella brachydactyla |
| Calcarius lapponicus |
| Carduelis carduelis, Carduelis citrinella, Carduelis spinus |
| Cecropis daurica |
| Cyanistes caeruleus, Cyanistes cyanus |
| Delichon urbicum |
| Dendrocopos minor |
| Emberiza aureola, Emberiza chrysophrys, Emberiza cia, Emberiza cirlus, Emberiza hortulana, Emberiza leucocephalos, Emberiza melanocephala, Emberiza pusilla, Emberiza rustica, Emberiza schoeniclus |
| Eremophila alpestris |
| Erithacus rubecula |
| Erythrina erythrina |
| Fringilla coelebs, Fringilla montifringilla |
| Hippolais icterina, Hippolais polyglotta |
| Hirundo rustica |
| Iduna caligata |
| Junco hyemalis |
| Larvivora sibilans |
| Linaria cannabina |
| Linaria flavirostris |
| Locustella certhiola, Locustella fluviatilis, Locustella luscinioides, Locustella naevia |
| Lophophanes cristatus |
| Lullula arborea |
| Luscinia megarhynchos, Luscinia svecica |
| Motacilla alba, Motacilla cinerea, Motacilla citreola, Motacilla flava |
| Muscicapa striata |
| Oenanthe deserti, Oenanthe hispanica, Oenanthe isabellina, Oenanthe leucopyga, Oenanthe oenanthe, Oenanthe pleschanka |
| Panurus biarmicus |
| Parus major |
| Passer montanus |
| Periparus ater |
| Phoenicurus ochruros, Phoenicurus phoenicurus |
| Poecile montanus, Poecile palustris |
| Prunella modularis, Prunella montanella |
| Remiz pendulinus |
| Riparia riparia |
| Saxicola maurus, Saxicola rubetra, Saxicola rubicola |
| Serinus serinus |
| Sylvia atricapilla, Sylvia borin, Sylvia cantillans, Sylvia communis, Sylvia curruca, Sylvia melanocephala |
| Tarsiger cyanurus |
| Troglodytes troglodytes P |


| Na (2.8; 5.0) |
|---|
| Emberiza citrinella |
| Luscinia luscinia |
| Montifringilla nivalis |
| Petronia petronia |
| Plectrophenax nivalis |
| Prunella collaris |
| Pyrrhula pyrrhula |
| Sylvia nisoria |


| Ns (2.8; 5.0) |
|---|
| Calidris minuta, Calidris minutilla, Calidris pusilla, Calidris temminckii |
| Passer domesticus |
| Phalaropus lobatus |


| Ja (3.3; 7.0) |
|---|
| Acrocephalus arundinaceus |
| Alauda arvensis, Alauda leucoptera |
| Bombycilla garrulus |
| Chloris chloris |
| Galerida cristata |
| Jynx torquilla |
| Lanius collurio, Lanius isabellinus, Lanius phoenicuroides, Lanius senator |
| Loxia curvirostra, Loxia leucoptera, Loxia pytyopsittacus |
| Melanocorypha calandra, Melanocorypha yeltoniensis |
| Pinicola enucleator |
| Sitta europaea |


| Js (3.3; 7.0) |
|---|
| Actitis hypoleucos, Actitis macularius |
| Calidris alba, Calidris alpina, Calidris bairdii, Calidris falcinellus, Calidris ferruginea, Calidris fuscicollis, Calidris melanotos, Calidris subruficollis |
| Charadrius alexandrinus, Charadrius dubius |
| Coccothraustes coccothraustes |
| Phalaropus fulicarius |
| Xenus cinereus |


| Ya (3.5; 3.5) |
|---|
| Alcedo atthis |


| Ys (3.5; 3.5) |
|---|
| Chlidonias hybrida, Chlidonias leucopterus, Chlidonias niger |
| Oceanites oceanicus |
| Sternula albifrons |


| Yxa (3.8; 3.5) |
|---|
| Apus apus, Apus pallidus |


| Ta (3.8; 7.0) |
|---|
| Emberiza calandra |
| Tichodroma muraria |
| Turdus eunomus, Turdus iliacus, Turdus naumanni, Turdus obscurus, Turdus philomelos |


| Ts (3.8; 7.0) |
|---|
| Calidris maritima |
| Charadrius hiaticula, Charadrius leschenaultii, Charadrius mongolus |
| Cinclus cinclus |
| Lymnocryptes minimus |
| Tringa glareola |


| Xxa (4.0; 4.0) |
|---|
| Ceryle rudis |
| Merops apiaster |
| Otus scops |
| Tachymarptis melba |


| Ha (4.2; 7.0) |
|---|
| Caprimulgus europaeus |
| Dendrocopos leucotos, Dendrocopos major, Dendrocopos medius, Dendrocopos syriacus |
| Geokichla sibirica |
| Lanius excubitor, Lanius minor |
| Monticola saxatilis |
| Oriolus oriolus |
| Pastor roseus |
| Picoides tridactylus |
| Sturnus vulgaris |
| Turdus atrogularis, Turdus merula, Turdus pilaris, Turdus ruficollis, Turdus torquatus, Turdus viscivorus |
| Upupa epops |
| Vireo olivaceus |
| Zoothera dauma |


| Hs (4.2; 7.0) |
|---|
| Arenaria interpres |
| Calidris canutus, Calidris pugnax F, Calidris tenuirostris |
| Charadrius morinellus |
| Coturnix coturnix |
| Gallinago gallinago |
| Hydrocoloeus minutus |
| Limnodromus scolopaceus |
| Porzana parva, Porzana pusilla |
| Rhodostethia rosea |
| Sterna dougallii, Sterna hirundo, Sterna paradisaea |
| Tringa melanoleuca, Tringa ochropus, Tringa stagnatilis |
| Xema sabini |


| Ga (5.5; 8.5) |
|---|
| Accipiter brevipes M, Accipiter nisus M |
| Coracias garrulus |
| Cuculus canorus |
| Picus canus, Picus viridis |


| Gs (5.5; 8.5) |
|---|
| Alle alle |
| Calidris pugnax M, P |
| Crex crex |
| Falco columbarius M |
| Gallinago media |
| Garrulus glandarius |
| Glareola nordmanni, Glareola pratincola |
| Himantopus himantopus |
| Lagopus lagopus |
| Limosa lapponica |
| Nucifraga caryocatactes |
| Pluvialis apricaria, Pluvialis dominica, Pluvialis fulva, Pluvialis squatarola |
| Porzana porzana |
| Rallus aquaticus |
| Tringa erythropus, Tringa nebularia, Tringa totanus |
| Vanellus gregarius, Vanellus leucurus, Vanellus vanellus |


| Pls (5.5; 10.0) |
|---|
| Gelochelidon nilotica |
| Sterna sandvicensis |


| Xa (6.0; 5.0) |
|---|
| Glaucidium passerinum |
| Streptopelia decaocto, Streptopelia orientalis, Streptopelia turtur |


| Fs (7.0; 8.5) |
|---|
| Accipiter brevipes F, Accipiter nisus F, P |
| Aegolius funereus |
| Athene noctua |
| Burhinus oedicnemus |
| Chroicocephalus genei, Chroicocephalus ridibundus |
| Circus cyaneus M, Circus macrourus, Circus pygargus |
| Columba oenas |
| Corvus monedula |
| Dryocopus martius |
| Falco columbarius F, Falco eleonorae, Falco naumanni, Falco subbuteo, Falco tinnunculus, Falco vespertinus |
| Ixobrychus minutus L |
| Larus atricilla, Larus canus, Larus delawarensis, Larus melanocephalus |
| Limosa limosa |
| Numenius phaeopus |
| Pagophila eburnea |
| Perdix perdix |
| Perisoreus infaustus |
| Pica pica |
| Pyrrhocorax graculus |
| Recurvirostra avosetta |
| Rissa tridactyla |
| Scolopax rusticola |
| Stercorarius longicaudus, Stercorarius parasiticus |
| Tetrastes bonasia |


| Fxa (8.0; 5.0) |
|---|
| Aegolius funereus |
| Athene noctua |


| Ea (9.0; 10.0) |
|---|
| Columba palumbus |


| Es (9.0; 10.0) |
|---|
| Circus aeruginosus M, Circus cyaneus F, P |
| Corvus cornix, Corvus corone, Corvus frugilegus |
| Fulmarus glacialis |
| Haematopus ostralegus |
| Hydroprogne caspia |
| Ixobrychus minutus P |
| Larus fuscus |
| Numenius arquata, Numenius tenuirostris |
| Stercorarius pomarinus, Stercorarius skua |


| Ps (9.0 x 6.0; 6.0) |
|---|
| Anas acuta, Anas americana, Anas carolinensis, Anas clypeata, Anas crecca, Anas discors, Anas penelope, Anas querquedula, Anas strepera |
| Aythya nyroca F |
| Cepphus grylle |
| Clangula hyemalis |
| Fratercula arctica |
| Gallinula chloropus |
| Histrionicus histrionicus |
| Mergellus albellus |
| Oxyura leucocephala |
| Podiceps auritus, Podiceps nigricollis |
| Podilymbus podiceps |
| Tachybaptus ruficollis |


| Da (11.0; 10.0) |
|---|
| Accipiter gentilis M |
| Asio flammeus, Asio otus |
| Buteo buteo, Buteo lagopus, Buteo rufinus |
| Elanus caeruleus |
| Falco cherrug, Falco peregrinus |
| Hieraaetus pennatus |
| Lyrurus tetrix F |
| Milvus migrans, Milvus milvus |
| Pernis apivorus |
| Strix aluco |
| Surnia ulula |
| Tyto alba |


| Ds (11.0; 10.0) |
|---|
| Ardeola ralloides |
| Bubulcus ibis |
| Circus aeruginosus F, P |
| Egretta garzetta |
| Larus argentatus, Larus cachinnans, Larus glaucoides, Larus hyperboreus, Larus ichthyaetus, Larus marinus, Larus michahellis, Larus schistisagus |
| Nycticorax nycticorax |


| Ws (12.0 x 22.0; 6.0) |
|---|
| Anser albifrons, Anser anser, Anser brachyrhynchus, Anser erythropus, Anser fabalis |
| Branta canadensis |
| Gavia adamsii, Gavia arctica, Gavia immer, Gavia stellata |
| Morus bassanus |
| Phalacrocorax aristotelis, Phalacrocorax carbo |


| Ca (13.0; 10.0) |
|---|
| Accipiter gentilis F |
| Falco rusticolus |
| Strix uralensis |


| Cs (13.0; 10.0) |
|---|
| Ardea alba, Ardea cinerea, Ardea purpurea |
| Botaurus stellaris |
| Corvus corax |
| Lyrurus tetrix M, P |
| Platalea leucorodia |
| Plegadis falcinellus |


| Ss (13.5 x 8.0; 6.0) |
|---|
| Alca torda |
| Anas platyrhynchos |
| Aythya affinis, Aythya collaris, Aythya ferina, Aythya fuligula, Aythya marila, Aythya nyroca P, M |
| Branta bernicla, Branta leucopsis, Branta ruficollis |
| Bucephala clangula |
| Fulica atra |
| Melanitta americana, Melanitta deglandi, Melanitta fusca, Melanitta nigra |
| Mergus merganser, Mergus serrator |
| Netta rufina |
| Phalacrocorax pygmeus |
| Podiceps cristatus, Podiceps grisegena |
| Polysticta stelleri |
| Somateria mollissima, Somateria spectabilis |
| Tadorna ferruginea, Tadorna tadorna |
| Uria aalge, Uria lomvia |


| Bxa (16.0; 20.0) |
|---|
| Aquila nipalensis |
| Circaetus gallicus |
| Clanga clanga , Clanga pomarina |
| Strix nebulosa |


| ELSA (16.0 x 21.0; 33.0) |
|---|
| Ciconia ciconia |


| Va (18,0; 20.0) |
|---|
| Ciconia ciconia, Ciconia nigra |
| Grus grus, Grus virgo |
| Pandion haliaetus |
| Phoenicopterus roseus |


| Aa (20.0 x 30.0; 20.0) |
|---|
| Aegypius monachus |
| Bubo bubo, Bubo scandiacus |
| Cygnus columbianus, Cygnus cygnus, Cygnus olor |
| Gypaetus barbatus |
| Gyps fulvus |
| Otis tarda |
| Pelecanus crispus, Pelecanus onocrotalus |


| Axa (22.0; 28,0) |
|---|
| Aquila chrysaetos, Aquila heliaca |
| Haliaeetus albicilla, Haliaeetus leucoryphus |
| Neophron percnopterus |
| Tetrao urogallus |

