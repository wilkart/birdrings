---
layout: post
title:  "Bird rings in Poland"
date:   2021-06-14 00:00:00
categories: birds
image: /images/pic09.jpg
redirect_from:
 - /bird-rings-in-poland/
 - /bird-rings-in-poland
 - /birds-rings-in-poland/
 - /birds-rings-in-poland
 - /bird-rings/
 - /bird-rings
---

As a Polish company and the producer of bird rings for [The Ornithological Station, Museum and Institute of Zoology Polish Academy of Sciences](http://www.stornit.gda.pl/index_en.php) we present a list of ring sizes used in Poland. 
The list is helpful to choose type and size of ring to fit on similar bird species in other regions.

<table class="table table-striped table-bordered">
<thead>
<tr>
<th>Type (inner diameter; height)</th>
<th>Material (form)</th>
</tr>
</thead>
<tbody>
<tr>
<td>La (2.0; 5.0)</td>
<td>Aluminium (split)</td>
</tr>
<tr>
<td>Ka (2.5; 5.0)</td>
<td>Aluminium (split)</td>
</tr>
<tr>
<td>Na (2.8; 5.0)</td>
<td>Aluminium (split)</td>
</tr>
<tr>
<td>Ns (2.8; 5.0)</td>
<td>Steel (split)</td>
</tr>
<tr>
<td>Ja (3.3; 7.0)</td>
<td>Aluminium (split)</td>
</tr>
<tr>
<td>Js (3.3; 7.0)</td>
<td>Steel (split)</td>
</tr>
<tr>
<td>Ya (3.5; 3.5)</td>
<td>Aluminium (split)</td>
</tr>
<tr>
<td>Ys (3.5; 3.5)</td>
<td>Steel (split)</td>
</tr>
<tr>
<td>Yxa (3.8; 3.5)</td>
<td>Steel (split)</td>
</tr>
<tr>
<td>Ta (3.8; 7.0)</td>
<td>Aluminium (split)</td>
</tr>
<tr>
<td>Ts (3.8; 7.0)</td>
<td>Steel (split)</td>
</tr>
<tr>
<td>Xxa (4.0; 4.0)</td>
<td>Aluminium (split)</td>
</tr>
<tr>
<td>Ha (4.2; 7.0)</td>
<td>Aluminium (split)</td>
</tr>
<tr>
<td>Hs (4.2; 7.0)</td>
<td>Steel (split)</td>
</tr>
<tr>
<td>Ga (5.5; 8.5)</td>
<td>Aluminium (split)</td>
</tr>
<tr>
<td>Gs (5.5; 8.5)</td>
<td>Steel (split)</td>
</tr>
<tr>
<td>Pls (5.5; 10.0)</td>
<td>Steel (split)</td>
</tr>
<tr>
<td>Xa (6.0; 5.0)</td>
<td>Aluminium (split)</td>
</tr>
<tr>
<td>Fs (7.0; 8.5)</td>
<td>Steel (split)</td>
</tr>
<tr>
<td>Fxa (8.0; 5.0)</td>
<td>Aluminium (split)</td>
</tr>
<tr>
<td>Ea (9.0; 10.0)</td>
<td>Aluminium (split)</td>
</tr>
<tr>
<td>Es (9.0; 10.0)</td>
<td>Steel (split)</td>
</tr>
<tr>
<td>Ps (9.0 x 6.0; 6.0)</td>
<td>Steel (with lock)</td>
</tr>
<tr>
<td>Da (11.0; 10.0)</td>
<td>Aluminium (with lock)</td>
</tr>
<tr>
<td>Ds (11.0; 10.0)</td>
<td>Steel (with lock)</td>
</tr>
<tr>
<td>Ws (12.0 x 22.0; 6.0)</td>
<td>Steel (with lock)</td>
</tr>
<tr>
<td>Ca (13.0; 10.0)</td>
<td>Aluminium (with lock)</td>
</tr>
<tr>
<td>Cs (13.0; 10.0)</td>
<td>Steel (with lock)</td>
</tr>
<tr>
<td>Ss (13.5 x 8.0; 6.0)</td>
<td>Steel (split)</td>
</tr>
<tr>
<td>Bxa (16.0; 20.0)</td>
<td>Aluminium (with lock)</td>
</tr>
<tr>
<td>ELSA (16.0 x 21.0; 33.0)</td>
<td>Steel (split)</td>
</tr>
<tr>
<td>Va (18,0; 20.0)</td>
<td>Aluminium (rivet)</td>
</tr>
<tr>
<td>Aa (20.0 x 30.0; 20.0)</td>
<td>Aluminium (with lock)</td>
</tr>
<tr>
<td>Axa (22.0; 28,0)</td>
<td>Aluminium (rivet)</td>
</tr>
</tbody>
</table>
