---
layout: post
title:  "Bat rings in Europe"
date:   2022-09-01 00:00:00
categories: bats
image: /images/pic09.jpg
redirect_from:
 - /bats-rings/
 - /bats-rings
 - /bat-rings/
 - /bat-rings
---

Ring sizes for European bat species recommended by [Eurobats](https://www.eurobats.org/). The ring sizes quoted represent the approximate internal diameter in mm of the oval at its widest point when the gap is closed to 1 mm. 2.9 mm rings are in the "narrow" design unless otherwise stated.

<table class="table table-striped table-bordered">
<thead>
<tr>
<th>Scientific name</th>
<th>Ring size</th>
</tr>
</thead>
<tbody>
<tr>
<td>Taphozous nudiventris</td>
<td>5.5 mm</td>
</tr>
<tr>
<td>Rhinolophus blasii</td>
<td>2.9/4.2 mm</td>
</tr>
<tr>
<td>Rhinolophus euryale</td>
<td>2.9/4.2 mm</td>
</tr>
<tr>
<td>Rhinolophus ferrumequinum</td>
<td>4.2 mm</td>
</tr>
<tr>
<td>Rhinolophus hipposideros</td>
<td>2.9 mm</td>
</tr>
<tr>
<td>Rhinolophus mehelyi</td>
<td>2.9/4.2 mm</td>
</tr>
<tr>
<td>Barbastella barbastellus</td>
<td>2.9 mm</td>
</tr>
<tr>
<td>Barbastella leucomelas</td>
<td>2.9 mm</td>
</tr>
<tr>
<td>Eptesicus bottae</td>
<td>2.9/4.2 mm</td>
</tr>
<tr>
<td>Eptesicus nilssonii</td>
<td>2.9 mm</td>
</tr>
<tr>
<td>Eptesicus serotinus</td>
<td>4.2/5.5 mm</td>
</tr>
<tr>
<td>Hypsugo savii</td>
<td>2.9 mm</td>
</tr>
<tr>
<td>Myotis alcathoe</td>
<td>2.9 mm</td>
</tr>
<tr>
<td>Myotis aurascens</td>
<td>2.9 mm</td>
</tr>
<tr>
<td>Myotis bechsteinii</td>
<td>2.9 mm</td>
</tr>
<tr>
<td>Myotis blythii</td>
<td>4.2/5.5 mm</td>
</tr>
<tr>
<td>Myotis brandtii</td>
<td>2.9 mm</td>
</tr>
<tr>
<td>Myotis capaccinii</td>
<td>2.9 mm</td>
</tr>
<tr>
<td>Myotis dasycneme</td>
<td>4.2 (3.5) mm</td>
</tr>
<tr>
<td>Myotis daubentonii</td>
<td>2.9 mm</td>
</tr>
<tr>
<td>Myotis emarginatus</td>
<td>2.9 mm</td>
</tr>
<tr>
<td>Myotis hajastanicus</td>
<td>2.9 mm</td>
</tr>
<tr>
<td>Myotis myotis</td>
<td>4.2/5.5 mm</td>
</tr>
<tr>
<td>Myotis mystacinus</td>
<td>2.9 mm</td>
</tr>
<tr>
<td>Myotis nattereri</td>
<td>2.9 mm</td>
</tr>
<tr>
<td>Myotis nipalensis</td>
<td>2.9 mm</td>
</tr>
<tr>
<td>Myotis schaubi</td>
<td>2.9/3.5 mm</td>
</tr>
<tr>
<td>Nyctalus lasiopterus</td>
<td>5.5 mm</td>
</tr>
<tr>
<td>Nyctalus leisleri</td>
<td>4.2/3.5 mm</td>
</tr>
<tr>
<td>Nyctalus noctula</td>
<td>4.2/3.5 mm</td>
</tr>
<tr>
<td>Otonycteris hemprichii</td>
<td>4.2 mm</td>
</tr>
<tr>
<td>Pipistrellus kuhlii</td>
<td>2.9 mm</td>
</tr>
<tr>
<td>Pipistrellus nathusii</td>
<td>2.9 mm</td>
</tr>
<tr>
<td>Pipistrellus pipistrellus</td>
<td>2.9/2.4 mm</td>
</tr>
<tr>
<td>Pipistrellus pygmaeus</td>
<td>2.9/2.4 mm</td>
</tr>
<tr>
<td>Plecotus alpinus</td>
<td>2.9 mm</td>
</tr>
<tr>
<td>Plecotus auritus</td>
<td>2.9 mm</td>
</tr>
<tr>
<td>Plecotus austriacus</td>
<td>2.9 mm</td>
</tr>
<tr>
<td>Plecotus kolombatovici</td>
<td>2.9 mm</td>
</tr>
<tr>
<td>Plecotus sardus</td>
<td>2.9 mm</td>
</tr>
<tr>
<td>Vespertilio murinus</td>
<td>4.2 mm</td>
</tr>
<tr>
<td>Miniopterus schreibersii</td>
<td>2.9/4.2 mm</td>
</tr>
<tr>
<td>Tadarida teniotis</td>
<td>4.2 mm</td>
</tr>
</tbody>
</table>

*Rousettus aegyptiacus* ringing is not appropriate and another marking technique will be identified.

