---
layout: no-sidebar
title: Duckling rings produced by Aranea
---

# Duckling rings

Special rings for day-old ducklings. These rings have the texture on the inside surface for plasticine filling. The edges of the rings are blunt. According to your requirements rings of other sizes can be produced. The rings are packed in plastic bags (10 pcs).

<h4>Table of sizes</h4>
<table>
<tr>
<th>Inner diameter (mm)</th>
<th>Height (mm)</th>
<th>Material</th>
<th>Features</th>
</tr>
<tr>
<td>9.4 &#215; 5.0</td>
<td>6</td>
<td>ST</td>
<td>TX</td>
</tr>
<tr>
<td>11.0 &#215; 6.2</td>
<td>6</td>
<td>ST</td>
<td>TX</td>
</tr>
<tr>
<td>12.0 &#215; 6.2</td>
<td>6</td>
<td>ST</td>
<td>TX</td>
</tr>
<tr>
<td>13.5 &#215; 6.9</td>
<td>6</td>
<td>ST</td>
<td>TX</td>
</tr>
<tr>
<td>15.2 &#215; 8.3</td>
<td>6</td>
<td>ST</td>
<td>TX</td>
</tr>
</table>

The letters code: 
- **ST** - stainless steel
- **TX** - with the texture on the inside surface for plasticine filling
