---
layout: no-sidebar
title: Round split rings by Aranea
---

# Round split rings

<span class="image right"><img src="images/rings-all.png" alt="" /></span>

Round split rings are open and ready to put on bird's leg. The edges of the rings are blunt. Small rings are packed on plastic tubes (100 pcs), large rings are packed in plastic bags (10 pcs).

<h4>Table of sizes</h4>
<table>
<tr>
<th>Inner diameter (mm)</th>
<th>Height (mm)</th>
<th>Material</th>
<th>Features</th>
</tr>
<tr>
<td>1.8</td>
<td>4</td>
<td>AL</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;&nbsp;&nbsp;1.8</td>
<td>5</td>
<td>AL</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2.0</td>
<td>4</td>
<td>AL</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;&nbsp;&nbsp;2.0</td>
<td>5</td>
<td>AL</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;&nbsp;&nbsp;2.0</td>
<td>5.5</td>
<td>AL</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2.3</td>
<td>5</td>
<td>AL, ST</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;&nbsp;&nbsp;2.3</td>
<td>5.5</td>
<td>AL</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2.5</td>
<td>3.5</td>
<td>AL</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;&nbsp;&nbsp;2.5</td>
<td>5</td>
<td>AL, ST</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;&nbsp;&nbsp;2.5</td>
<td>5.5</td>
<td>AL</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2.8</td>
<td>5</td>
<td>AL, ST</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;&nbsp;&nbsp;2.8</td>
<td>5.5</td>
<td>AL</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;&nbsp;&nbsp;2.8</td>
<td>6</td>
<td>AL</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3.0</td>
<td>3.5</td>
<td>AL</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;&nbsp;&nbsp;3.0</td>
<td>5</td>
<td>AL, ST</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;&nbsp;&nbsp;3.0</td>
<td>5.5</td>
<td>AL</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3.3</td>
<td>5</td>
<td>AL, ST</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;&nbsp;&nbsp;3.3</td>
<td>5.5</td>
<td>AL</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;&nbsp;&nbsp;3.3</td>
<td>7</td>
<td>AL, ST</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3.5</td>
<td>3.5</td>
<td>AL, ST</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;&nbsp;&nbsp;3.5</td>
<td>4.5</td>
<td>AL, ST</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;&nbsp;&nbsp;3.5</td>
<td>5.5</td>
<td>AL</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;&nbsp;&nbsp;3.5</td>
<td>6</td>
<td>AL</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;&nbsp;&nbsp;3.5</td>
<td>7</td>
<td>AL</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3.8</td>
<td>3.5</td>
<td>AL</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;&nbsp;&nbsp;3.8</td>
<td>4.5</td>
<td>AL</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;&nbsp;&nbsp;3.8</td>
<td>7</td>
<td>AL, ST</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>4.0</td>
<td>4</td>
<td>AL, ST</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;&nbsp;&nbsp;4.0</td>
<td>4.5</td>
<td>AL</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;&nbsp;&nbsp;4.0</td>
<td>5.5</td>
<td>AL</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;&nbsp;&nbsp;4.0</td>
<td>6</td>
<td>AL</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;&nbsp;&nbsp;4.0</td>
<td>7</td>
<td>AL, ST</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>4.2</td>
<td>7</td>
<td>AL, ST</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>4.3</td>
<td>7</td>
<td>AL, ST</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>4.5</td>
<td>7</td>
<td>AL, ST</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>5.0</td>
<td>7</td>
<td>AL, ST</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;&nbsp;&nbsp;5.0</td>
<td>10</td>
<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ST</td>
<td>DB, easily readable, for Sterna</td>
</tr>
<tr>
<td>5.25</td>
<td>7</td>
<td>AL, ST</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>5.5</td>
<td>4.5</td>
<td>AL, ST</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;&nbsp;&nbsp;5.5</td>
<td>7</td>
<td>AL, ST</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;&nbsp;&nbsp;5.5</td>
<td>8.5</td>
<td>AL, ST</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;&nbsp;&nbsp;5.5</td>
<td>10</td>
<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ST</td>
<td>DB, easily readable, for Sterna</td>
</tr>
<tr>
<td>6.0</td>
<td>5</td>
<td>AL, ST</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;&nbsp;&nbsp;6.0</td>
<td>7</td>
<td>AL, ST</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;&nbsp;&nbsp;6.0</td>
<td>9</td>
<td>AL, ST</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;&nbsp;&nbsp;6.0</td>
<td>10</td>
<td>AL, ST</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;&nbsp;&nbsp;6.0</td>
<td>10</td>
<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ST</td>
<td>DB, easily readable, for Larus</td>
</tr>
<tr>
<td>6.5</td>
<td>5</td>
<td>AL</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;&nbsp;&nbsp;6.5</td>
<td>7</td>
<td>AL, ST</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;&nbsp;&nbsp;6.5</td>
<td>8.5</td>
<td>AL, ST</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;&nbsp;&nbsp;6.5</td>
<td>9</td>
<td>AL, ST</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;&nbsp;&nbsp;6.5</td>
<td>10</td>
<td>AL, ST</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>7.0</td>
<td>7</td>
<td>AL, ST</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;&nbsp;&nbsp;7.0</td>
<td>8.5</td>
<td>AL, ST</td>
<td>V</td>
</tr>
<tr>
<td>&nbsp;&nbsp;&nbsp;7.0</td>
<td>9</td>
<td>AL, ST</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;&nbsp;&nbsp;7.0</td>
<td>10</td>
<td>AL, ST</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;&nbsp;&nbsp;7.0</td>
<td>12</td>
<td>AL, ST</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>8.0</td>
<td>5</td>
<td>AL</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;&nbsp;&nbsp;8.0</td>
<td>9</td>
<td>AL, ST</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;&nbsp;&nbsp;8.0</td>
<td>10</td>
<td>AL, ST</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>9.0</td>
<td>9</td>
<td>AL, ST</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;&nbsp;&nbsp;9.0</td>
<td>10</td>
<td>AL, ST</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>9.5</td>
<td>12</td>
<td>AL, ST</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>10.0</td>
<td>9</td>
<td>AL, ST</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;&nbsp;&nbsp;10.0</td>
<td>10</td>
<td>AL, ST</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;&nbsp;&nbsp;10.0</td>
<td>12</td>
<td>AL, ST</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>11.0</td>
<td>7</td>
<td>AL</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;&nbsp;&nbsp;11.0</td>
<td>8</td>
<td>AL</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;&nbsp;&nbsp;11.0</td>
<td>10</td>
<td>AL, ST</td>
<td>V</td>
</tr>
<tr>
<td>&nbsp;&nbsp;&nbsp;11.0</td>
<td>12</td>
<td>AL, ST</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>12.0</td>
<td>10</td>
<td>AL, ST</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>13.0</td>
<td>10</td>
<td>AL, ST</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>14.0</td>
<td>10</td>
<td>AL, ST</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;&nbsp;&nbsp;14.0</td>
<td>12</td>
<td>AL, ST</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>16.0</td>
<td>10</td>
<td>AL, ST</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;&nbsp;&nbsp;16.0</td>
<td>12</td>
<td>AL, ST</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>18.0</td>
<td>10</td>
<td>AL</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;&nbsp;&nbsp;18.0</td>
<td>12</td>
<td>AL</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;&nbsp;&nbsp;18.0</td>
<td>20</td>
<td>AL</td>
<td>DB</td>
</tr>
<tr>
<td>&nbsp;&nbsp;&nbsp;18.0</td>
<td>25</td>
<td>AL</td>
<td>DB</td>
</tr>
<tr>
<td>19.0</td>
<td>12</td>
<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ST</td>
<td>V</td>
</tr>
</table>

The letters code:
- **AL** - pure aluminium or aluminium-magnesium-manganese alloy
- **ST** - stainless steel
- **DB** - double number
- **V** - V-shape available
