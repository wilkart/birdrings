---
layout: no-sidebar
title: Bat rings produced by Aranea
---

# Bat rings

**Rings of suitable size for bats**. Putting uniquely numbered rings on wild bats to identify individuals is a long established technique associated with important bat research and monitoring. 
- The rings are made from very light materials.
- The rings are designed to have no adverse effect on the bats.
- Round lipped rings are open and ready to put on bat's arm. 
- The edges of the rings are blunt. Rings are packed on plastic tubes.

<table class="table table-striped table-bordered">
<thead>
<tr>
<th>Inner diameter</th>
<th>Height</th>
<th>Material</th>
</tr>
</thead>
<tbody>
<tr>
<td>2.4 mm</td>
<td>4.0 mm</td>
<td>Aluminium-magnesium-manganese alloy</td>
</tr>
<tr>
<td>2.4 mm</td>
<td>5.0 mm</td>
<td>Aluminium-magnesium-manganese alloy</td>
</tr>
<tr>
<td>2.9 mm</td>
<td>4.0 mm</td>
<td>Aluminium-magnesium-manganese alloy</td>
</tr>
<tr>
<td>2.9 mm</td>
<td>5.0 mm</td>
<td>Aluminium-magnesium-manganese alloy</td>
</tr>
<tr>
<td>3.5 mm</td>
<td>5.0 mm</td>
<td>Aluminium-magnesium-manganese alloy</td>
</tr>
<tr>
<td>4.2 mm</td>
<td>5.0 mm</td>
<td>Aluminium-magnesium-manganese alloy</td>
</tr>
<tr>
<td>5.2 mm</td>
<td>5.0 mm</td>
<td>Aluminium-magnesium-manganese alloy</td>
</tr>
<tr>
<td>5.5 mm</td>
<td>5.5 mm</td>
<td>Stainless steel</td>
</tr>
</tbody>
</table>
